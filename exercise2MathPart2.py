import torch
from torchvision.models import resnet18, ResNet18_Weights

model = resnet18(weights=ResNet18_Weights.DEFAULT)

if torch.cuda.is_available():
    model = model.to('cuda')

print(f"Our model with no changes whatsoever:\n{model}\n")

for param in model.parameters():
    param.requires_grad = False

print(f"Our model without requiring gradients:\n{model}\n")

model.fc = torch.nn.Linear(512, 10)

print(f"Our model with a new, unfrozen last linear layer:\n{model}\n")

optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

print(f"Our optimizer:\n{optimizer}")

