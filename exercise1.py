import numpy as np
import torch

data = [[1, 2], [3, 4], [5, 6], [7, 8]]
xData = torch.tensor(data)

npArray = np.array(data)
xNpArray = torch.from_numpy(npArray)
print(f"Numpy tensor:\n{xNpArray}\n")

xOnes = torch.ones_like(xData)
print(f"Ones tensor:\n{xOnes}\n")

xRand = torch.rand_like(xData, dtype=torch.float32)
print(f"Random tensor:\n{xRand}")

shape = (2, 3)
randTensor = torch.rand(shape)
onesTensor = torch.ones(shape)
zerosTensor = torch.zeros(shape)

print(f"Random tensor:\n{randTensor}\n")
print(f"Ones tensor:\n{onesTensor}\n")
print(f"Zeros tensor:\n{zerosTensor}\n")

tensor = torch.rand(3, 4)

print(f"Shape of tensor:\n{tensor.shape}")
print(f"Datatype of tensor:\n{tensor.dtype}")
print(f"Device tensor is stored on:\n{tensor.device}")

if torch.cuda.is_available():
    tensor = tensor.to('cuda')
    print(f"Device tensor is stored on:\n{tensor.device}\n")
else:
    print("No CUDA available. See for yourself:")
    print(torch.cuda.is_available())
    print("\n")

squareOneTensor = torch.ones(4, 4)
if torch.cuda.is_available():
    squareOneTensor = squareOneTensor.to('cuda')
print(f"Square one tensor:\n{squareOneTensor}\n")
squareOneTensor[:, 1] = 0
print(f"Square one tensor after slicing:\n{squareOneTensor}\n")

longTensor = torch.cat([squareOneTensor, squareOneTensor, squareOneTensor], dim=1)
print(f"3 square long tensors concatenated:\n{longTensor}\n")

print("Tensor multiplication (element-wise)")
print(f"tensor.mul(tensor):\n{tensor.mul(tensor)}\n")
print(f"tensor * tensor:\n{tensor * tensor}\n")

print(f"In-place operations:")
print(tensor, "\n")
tensor.add_(5)
print(f"Added 5 to tensor:\n {tensor}\n")

if torch.cuda.is_available():
    print("Copying our tensor back to the CPU for further operations")
    tensor = tensor.cpu()

print("Torch to numpy")
print(f"Our tensor:\n{tensor}")
numpyedTensor = tensor.numpy()
print(f"Numpyed Tensor (a.k.a. NumPy Array):\n{numpyedTensor}\n")

print("Changing the tensor changes the array:")
print("Adding 1 to the tensor and hence, to the array")
tensor.add_(1)
print(f"Tensor:\n{tensor}\n")
print(f"Numpyed Tensor:\n{numpyedTensor}\n")

print("Changing the Array changes its associated tensor:")
print("Adding one to our random numpy array from the beginning")
print(f"For reference:\n{npArray}")
np.add(npArray, 1, out=npArray)
print(f"Numpy array:\n{npArray}\n")
print(f"Associated tensor:\n{xNpArray}")
