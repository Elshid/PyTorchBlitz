import torch
from torchvision.models import resnet18, ResNet18_Weights

model = resnet18(weights=ResNet18_Weights.DEFAULT)
data = torch.rand(1, 3, 64, 64)
labels = torch.rand(1, 1000)

if torch.cuda.is_available():
    data = data.to('cuda')
    labels = labels.to('cuda')
    model = model.to('cuda')

print(f"The data:\n{data}\n")
print(f"The labels:\n{labels}\n")

print("Forward pass...")
prediction = model(data)

print("Loss and backward pass...")
loss = (prediction - labels).sum()
loss.backward()

print("Loading optimizer...")
optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
optimizer.step()

print(f"Resulting model:\n{model}\n")
