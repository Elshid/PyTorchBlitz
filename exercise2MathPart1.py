import torch

a = torch.tensor([1., 2., 3., 4., 5., 6., 7., 8.], requires_grad=True)
b = torch.tensor([9., 8., 7., 6., 5., 4., 3., 2.], requires_grad=True)

if torch.cuda.is_available():
    a.to('cuda')
    b.to('cuda')

print("Our tensors:")
print(f"a:\n{a}\n")
print(f"b\n{b}\n")

Q = 3*a**3 - b**2
print(f"The error:\n {Q}\n")

externalGrad = torch.tensor([1., 1., 1., 1., 1., 1., 1., 1.])
if torch.cuda.is_available():
    externalGrad.to('cuda')

Q.backward(gradient=externalGrad)

print("Our gradients:")
print(f"Gradients of a:\n{a.grad}\n")
print(f"Gradients of b:\n{b.grad}\n")
print("Check if our grads are correct")
print(9*a**2 == a.grad)
print(-2*b == b.grad)

print("Which of the following tensors require grad?")
x = torch.rand((5, 5), requires_grad=False)
y = torch.rand((5, 5), requires_grad=False)
z = torch.rand((5, 5), requires_grad=True)

if torch.cuda.is_available():
    x.to('cuda')
    y.to('cuda')
    z.to('cuda')


xyTensor = x + y
yzTensor = y + z

print(f"Does a tensor built from tensors requiring no grads require grads?\n{xyTensor.requires_grad}")
print(f"Does a tensor built partially from tensors requiring grads require grads?\n{yzTensor.requires_grad}")
