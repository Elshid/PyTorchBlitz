import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


class NeuralNet(nn.Module):

    def __init__(self):
        super(NeuralNet, self).__init__()
        # 1 input image channel, 6 output channels, 5x5 square convolution
        # The kernel
        self.conv1 = nn.Conv2d(1, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        # Affine operations: y = Wx + b
        self.fc1 = nn.Linear(16 * 5 * 5, 120)  # 5 x 5 image dimensions
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # Max pooling over a (2, 2) window
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        # If the site is a square, specify it with a single number
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = torch.flatten(x, 1)  # Flatten all dimensions except of batch dimension
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


net = NeuralNet()
nnInput = torch.randn(1, 1, 32, 32)
gradients = torch.randn(1, 10)
target = torch.randn(10)

if torch.cuda.is_available():
    net = net.to('cuda')
    nnInput = nnInput.to('cuda')
    gradients = gradients.to('cuda')
    target = target.to('cuda')
    print("Net should be on CUDA")

print(net)

parameters = list(net.parameters())
print(f"The number of parameters in out network:\n{len(parameters)}\n")
print(f"The weight of conv1:\n{parameters[0].size()}\n")

out = net(nnInput)
print(f"The output of our Neural Network:\n{out}\n")

net.zero_grad()
out.backward(gradients)

output = net(nnInput)
target = target.view(1, -1)
criterion = nn.MSELoss()

loss = criterion(output, target)
print(f"The loss is:\n{loss}\n")

print(f"The MSELoss of our loss:\n{loss.grad_fn}")
print(f"The linear loss of our loss:\n{loss.grad_fn.next_functions[0][0]}")
print(f"The loss of RELu of our loss:\n{loss.grad_fn.next_functions[0][0].next_functions[0][0]}")

net.zero_grad()

print(f"The gradients of the bias of conv1 before our backward propagation:\n{net.conv1.bias.grad}")

loss.backward()

print(f"The gradients of the bias of conv1 after our backward propagation:\n{net.conv1.bias.grad}")

learningRate = 0.01
for f in net.parameters():
    f.data.sub_(f.grad.data * learningRate)

print("Creating optimizer...")
optimizer = optim.SGD(net.parameters(), lr=learningRate)

optimizer.zero_grad()
output = net(nnInput)
loss = criterion(output, target)
loss.backward()
optimizer.step()
