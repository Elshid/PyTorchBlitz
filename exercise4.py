import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import sys


class Net(nn.Module):

    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 192, 3, 1, 1)
        self.relu1 = nn.RReLU()
        self.relu2 = nn.RReLU()
        self.relu3 = nn.RReLU()
        self.relu4 = nn.RReLU()
        self.relu5 = nn.RReLU()
        self.pool1 = nn.MaxPool2d(2, 2)
        self.pool2 = nn.MaxPool2d(2, 2)
        self.pool3 = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(192, 128, 5, 1, 2)
        self.conv3 = nn.Conv2d(128, 256, 7, 1, 3)
        self.fc1 = nn.Linear(64 * 8 * 8, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool1(self.relu1(self.conv1(x)))
        x = self.pool2(self.relu2(self.conv2(x)))
        x = self.pool3(self.relu3(self.conv3(x)))
        x = torch.flatten(x, 1)  # flatten all dimensions except batch dimension
        x = self.relu4(self.fc1(x))
        x = self.relu5(self.fc2(x))
        x = self.fc3(x)
        return x


def imshow(img):
    if len(sys.argv) < 2 or sys.argv[1] != "CI":
        img = img / 2 + 0.5
        npImg = img.numpy()
        plt.imshow(np.transpose(npImg, (1, 2, 0)))
        plt.show()


transformer = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

batchSize = 4

trainset = torchvision.datasets.CIFAR10(root='./data', train=True, download=True, transform=transformer)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batchSize, shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transformer)
testloader = torch.utils.data.DataLoader(testset, batch_size=batchSize, shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

print("Getting some random training images")
imgIter = iter(trainloader)
images, labels = next(imgIter)

imshow(torchvision.utils.make_grid(images))
print(' '.join(f"{classes[labels[j]]:5s}" for j in range(batchSize)))

net = Net()

device = 'cpu'

if torch.cuda.is_available():
    net = net.to('cuda')
    device = 'cuda'

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

numEpochs = 10

if len(sys.argv) >= 2 and sys.argv[1] == "CI":
    numEpochs = 2

for epoch in range(numEpochs):

    runningLoss = 0.0
    for i, data in enumerate(trainloader, 0):
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()

        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        runningLoss += loss.item()
        if i % 2000 == 0:
            print(f"[{epoch}, {i:5d}] loss: {runningLoss / 2000:.3f}")
            runningLoss = 0

print("Done training")

PATH = "./CIFARNet.pth"
torch.save(net.state_dict(), PATH)

dataIter = iter(testloader)
images, labels = next(dataIter)

imshow(torchvision.utils.make_grid(images))
print("Ground truth: ", " ".join(f"{classes[labels[j]]:5s}" for j in range(batchSize)))

loadedNet = Net()
loadedNet = loadedNet.load_state_dict(torch.load(PATH))

print("Loaded Net")
print(loadedNet)
print("Trained Net")
print(net)

if torch.cuda.is_available():
#    loadedNet = loadedNet.to('cuda')
    images = images.to('cuda')

outputs = net(images)
_, predicted = torch.max(outputs, 1)

print("Predicted: ", ' '.join(f"{classes[predicted[j]]:5s}" for j in range(batchSize)))

correct = 0
total = 0
# As we are not training, we don't calculate the gradients
with torch.no_grad():
    for data in testloader:
        images, labels = data
        images = images.to(device)
        labels = labels.to(device)
        # Calculate outputs by running images through the network
        outputs = net(images)
        # The class with the highest energy is what we choose as prediction
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

    print(f"Accuracy of the network in 10000 test images: {100 * correct // total} %")

print("Prepare to count predictions for each class")
correctPred = {classname: 0 for classname in classes}
totalPred = {classname: 0 for classname in classes}

with torch.no_grad():
    for data in testloader:
        images, labels = data
        images = images.to(device)
        labels = labels.to(device)
        outputs = net(images)
        _, predictions = torch.max(outputs.data, 1)
        for label, prediction in zip(labels, predictions):
            if label == prediction:
                correctPred[classes[label]] += 1
            totalPred[classes[label]] += 1

for classname, correctCount in correctPred.items():
    accuracy = 100 * float(correctCount) / totalPred[classname]
    print(f"Accuracy for class {classname:5s} is {accuracy:.1f} %")
